﻿namespace DSPFishingAreas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ZoneList = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.AreaList = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.NewAreaName = new System.Windows.Forms.TextBox();
            this.NewAreaID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.AddAreaBtn = new System.Windows.Forms.Button();
            this.RemoveAreaBtn = new System.Windows.Forms.Button();
            this.AreaDetailsGroup = new System.Windows.Forms.GroupBox();
            this.SaveAreaBtn = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.FishingAreaBounds = new System.Windows.Forms.DataGridView();
            this.X = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Z = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FishingAreaRadius = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.FishingAreaHeight = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.FishingAreaCenterZ = new System.Windows.Forms.TextBox();
            this.FishingAreaCenterY = new System.Windows.Forms.TextBox();
            this.FishingAreaCenterX = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.BoundTypePolygon = new System.Windows.Forms.RadioButton();
            this.BoundTypeRadius = new System.Windows.Forms.RadioButton();
            this.BoundTypeZone = new System.Windows.Forms.RadioButton();
            this.AreaID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.AreaName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SeaWater = new System.Windows.Forms.RadioButton();
            this.FreshWater = new System.Windows.Forms.RadioButton();
            this.AreaDetailsGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FishingAreaBounds)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ZoneList
            // 
            this.ZoneList.FormattingEnabled = true;
            this.ZoneList.Location = new System.Drawing.Point(12, 29);
            this.ZoneList.Name = "ZoneList";
            this.ZoneList.Size = new System.Drawing.Size(148, 290);
            this.ZoneList.TabIndex = 0;
            this.ZoneList.SelectedValueChanged += new System.EventHandler(this.ZoneList_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Zone";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(163, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Area";
            // 
            // AreaList
            // 
            this.AreaList.FormattingEnabled = true;
            this.AreaList.Location = new System.Drawing.Point(166, 29);
            this.AreaList.Name = "AreaList";
            this.AreaList.Size = new System.Drawing.Size(197, 290);
            this.AreaList.TabIndex = 2;
            this.AreaList.SelectedValueChanged += new System.EventHandler(this.AreaList_SelectedValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 362);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Area Name";
            // 
            // NewAreaName
            // 
            this.NewAreaName.Location = new System.Drawing.Point(78, 359);
            this.NewAreaName.Name = "NewAreaName";
            this.NewAreaName.Size = new System.Drawing.Size(180, 20);
            this.NewAreaName.TabIndex = 5;
            // 
            // NewAreaID
            // 
            this.NewAreaID.Location = new System.Drawing.Point(78, 385);
            this.NewAreaID.Name = "NewAreaID";
            this.NewAreaID.Size = new System.Drawing.Size(34, 20);
            this.NewAreaID.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 388);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Area ID";
            // 
            // AddAreaBtn
            // 
            this.AddAreaBtn.Location = new System.Drawing.Point(141, 413);
            this.AddAreaBtn.Name = "AddAreaBtn";
            this.AddAreaBtn.Size = new System.Drawing.Size(75, 23);
            this.AddAreaBtn.TabIndex = 8;
            this.AddAreaBtn.Text = "Add Area";
            this.AddAreaBtn.UseVisualStyleBackColor = true;
            this.AddAreaBtn.Click += new System.EventHandler(this.AddAreaBtn_Click);
            // 
            // RemoveAreaBtn
            // 
            this.RemoveAreaBtn.Location = new System.Drawing.Point(207, 325);
            this.RemoveAreaBtn.Name = "RemoveAreaBtn";
            this.RemoveAreaBtn.Size = new System.Drawing.Size(120, 23);
            this.RemoveAreaBtn.TabIndex = 9;
            this.RemoveAreaBtn.Text = "Remove Area";
            this.RemoveAreaBtn.UseVisualStyleBackColor = true;
            this.RemoveAreaBtn.Click += new System.EventHandler(this.RemoveAreaBtn_Click);
            // 
            // AreaDetailsGroup
            // 
            this.AreaDetailsGroup.Controls.Add(this.panel1);
            this.AreaDetailsGroup.Controls.Add(this.label9);
            this.AreaDetailsGroup.Controls.Add(this.SaveAreaBtn);
            this.AreaDetailsGroup.Controls.Add(this.label8);
            this.AreaDetailsGroup.Controls.Add(this.label29);
            this.AreaDetailsGroup.Controls.Add(this.FishingAreaBounds);
            this.AreaDetailsGroup.Controls.Add(this.FishingAreaRadius);
            this.AreaDetailsGroup.Controls.Add(this.label28);
            this.AreaDetailsGroup.Controls.Add(this.FishingAreaHeight);
            this.AreaDetailsGroup.Controls.Add(this.label27);
            this.AreaDetailsGroup.Controls.Add(this.FishingAreaCenterZ);
            this.AreaDetailsGroup.Controls.Add(this.FishingAreaCenterY);
            this.AreaDetailsGroup.Controls.Add(this.FishingAreaCenterX);
            this.AreaDetailsGroup.Controls.Add(this.label26);
            this.AreaDetailsGroup.Controls.Add(this.label25);
            this.AreaDetailsGroup.Controls.Add(this.label24);
            this.AreaDetailsGroup.Controls.Add(this.label7);
            this.AreaDetailsGroup.Controls.Add(this.BoundTypePolygon);
            this.AreaDetailsGroup.Controls.Add(this.BoundTypeRadius);
            this.AreaDetailsGroup.Controls.Add(this.BoundTypeZone);
            this.AreaDetailsGroup.Controls.Add(this.AreaID);
            this.AreaDetailsGroup.Controls.Add(this.label5);
            this.AreaDetailsGroup.Controls.Add(this.AreaName);
            this.AreaDetailsGroup.Controls.Add(this.label6);
            this.AreaDetailsGroup.Enabled = false;
            this.AreaDetailsGroup.Location = new System.Drawing.Point(369, 13);
            this.AreaDetailsGroup.Name = "AreaDetailsGroup";
            this.AreaDetailsGroup.Size = new System.Drawing.Size(302, 502);
            this.AreaDetailsGroup.TabIndex = 10;
            this.AreaDetailsGroup.TabStop = false;
            this.AreaDetailsGroup.Text = "Area Details";
            // 
            // SaveAreaBtn
            // 
            this.SaveAreaBtn.Location = new System.Drawing.Point(112, 471);
            this.SaveAreaBtn.Name = "SaveAreaBtn";
            this.SaveAreaBtn.Size = new System.Drawing.Size(75, 23);
            this.SaveAreaBtn.TabIndex = 56;
            this.SaveAreaBtn.Text = "Save Area";
            this.SaveAreaBtn.UseVisualStyleBackColor = true;
            this.SaveAreaBtn.Click += new System.EventHandler(this.SaveAreaBtn_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(116, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 55;
            this.label8.Text = "Area Center";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(85, 222);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(128, 15);
            this.label29.TabIndex = 54;
            this.label29.Text = "Boundary Poly Vectors";
            // 
            // FishingAreaBounds
            // 
            this.FishingAreaBounds.AllowUserToResizeRows = false;
            this.FishingAreaBounds.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.FishingAreaBounds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FishingAreaBounds.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.X,
            this.Z});
            this.FishingAreaBounds.Location = new System.Drawing.Point(31, 245);
            this.FishingAreaBounds.MultiSelect = false;
            this.FishingAreaBounds.Name = "FishingAreaBounds";
            this.FishingAreaBounds.Size = new System.Drawing.Size(244, 220);
            this.FishingAreaBounds.TabIndex = 53;
            // 
            // X
            // 
            this.X.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X.DefaultCellStyle = dataGridViewCellStyle1;
            this.X.HeaderText = "X";
            this.X.Name = "X";
            this.X.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Z
            // 
            this.Z.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Z.DefaultCellStyle = dataGridViewCellStyle2;
            this.Z.HeaderText = "Z";
            this.Z.Name = "Z";
            this.Z.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FishingAreaRadius
            // 
            this.FishingAreaRadius.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FishingAreaRadius.Location = new System.Drawing.Point(212, 194);
            this.FishingAreaRadius.Name = "FishingAreaRadius";
            this.FishingAreaRadius.Size = new System.Drawing.Size(35, 21);
            this.FishingAreaRadius.TabIndex = 52;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(160, 197);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 15);
            this.label28.TabIndex = 51;
            this.label28.Text = "Radius";
            // 
            // FishingAreaHeight
            // 
            this.FishingAreaHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FishingAreaHeight.Location = new System.Drawing.Point(111, 194);
            this.FishingAreaHeight.Name = "FishingAreaHeight";
            this.FishingAreaHeight.Size = new System.Drawing.Size(35, 21);
            this.FishingAreaHeight.TabIndex = 50;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(62, 197);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 15);
            this.label27.TabIndex = 49;
            this.label27.Text = "Height";
            // 
            // FishingAreaCenterZ
            // 
            this.FishingAreaCenterZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FishingAreaCenterZ.Location = new System.Drawing.Point(216, 167);
            this.FishingAreaCenterZ.Name = "FishingAreaCenterZ";
            this.FishingAreaCenterZ.Size = new System.Drawing.Size(58, 21);
            this.FishingAreaCenterZ.TabIndex = 48;
            // 
            // FishingAreaCenterY
            // 
            this.FishingAreaCenterY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FishingAreaCenterY.Location = new System.Drawing.Point(132, 167);
            this.FishingAreaCenterY.Name = "FishingAreaCenterY";
            this.FishingAreaCenterY.Size = new System.Drawing.Size(58, 21);
            this.FishingAreaCenterY.TabIndex = 47;
            // 
            // FishingAreaCenterX
            // 
            this.FishingAreaCenterX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FishingAreaCenterX.Location = new System.Drawing.Point(48, 167);
            this.FishingAreaCenterX.Name = "FishingAreaCenterX";
            this.FishingAreaCenterX.Size = new System.Drawing.Size(58, 21);
            this.FishingAreaCenterX.TabIndex = 46;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(196, 170);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(14, 15);
            this.label26.TabIndex = 45;
            this.label26.Text = "Z";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(112, 170);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(14, 15);
            this.label25.TabIndex = 44;
            this.label25.Text = "Y";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(27, 170);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(15, 15);
            this.label24.TabIndex = 43;
            this.label24.Text = "X";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(108, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Boundary Type";
            // 
            // BoundTypePolygon
            // 
            this.BoundTypePolygon.AutoSize = true;
            this.BoundTypePolygon.Location = new System.Drawing.Point(193, 127);
            this.BoundTypePolygon.Name = "BoundTypePolygon";
            this.BoundTypePolygon.Size = new System.Drawing.Size(63, 17);
            this.BoundTypePolygon.TabIndex = 14;
            this.BoundTypePolygon.Text = "Polygon";
            this.BoundTypePolygon.UseVisualStyleBackColor = true;
            // 
            // BoundTypeRadius
            // 
            this.BoundTypeRadius.AutoSize = true;
            this.BoundTypeRadius.Location = new System.Drawing.Point(129, 127);
            this.BoundTypeRadius.Name = "BoundTypeRadius";
            this.BoundTypeRadius.Size = new System.Drawing.Size(58, 17);
            this.BoundTypeRadius.TabIndex = 13;
            this.BoundTypeRadius.Text = "Radius";
            this.BoundTypeRadius.UseVisualStyleBackColor = true;
            // 
            // BoundTypeZone
            // 
            this.BoundTypeZone.AutoSize = true;
            this.BoundTypeZone.Checked = true;
            this.BoundTypeZone.Location = new System.Drawing.Point(39, 127);
            this.BoundTypeZone.Name = "BoundTypeZone";
            this.BoundTypeZone.Size = new System.Drawing.Size(84, 17);
            this.BoundTypeZone.TabIndex = 12;
            this.BoundTypeZone.TabStop = true;
            this.BoundTypeZone.Text = "Whole Zone";
            this.BoundTypeZone.UseVisualStyleBackColor = true;
            // 
            // AreaID
            // 
            this.AreaID.Location = new System.Drawing.Point(78, 45);
            this.AreaID.Name = "AreaID";
            this.AreaID.Size = new System.Drawing.Size(34, 20);
            this.AreaID.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Area ID";
            // 
            // AreaName
            // 
            this.AreaName.Location = new System.Drawing.Point(78, 19);
            this.AreaName.Name = "AreaName";
            this.AreaName.Size = new System.Drawing.Size(180, 20);
            this.AreaName.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Area Name";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(116, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 57;
            this.label9.Text = "Water Type";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.SeaWater);
            this.panel1.Controls.Add(this.FreshWater);
            this.panel1.Location = new System.Drawing.Point(56, 86);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 19);
            this.panel1.TabIndex = 58;
            // 
            // SeaWater
            // 
            this.SeaWater.AutoSize = true;
            this.SeaWater.Location = new System.Drawing.Point(115, 1);
            this.SeaWater.Name = "SeaWater";
            this.SeaWater.Size = new System.Drawing.Size(44, 17);
            this.SeaWater.TabIndex = 61;
            this.SeaWater.Text = "Sea";
            this.SeaWater.UseVisualStyleBackColor = true;
            // 
            // FreshWater
            // 
            this.FreshWater.AutoSize = true;
            this.FreshWater.Checked = true;
            this.FreshWater.Location = new System.Drawing.Point(41, 1);
            this.FreshWater.Name = "FreshWater";
            this.FreshWater.Size = new System.Drawing.Size(51, 17);
            this.FreshWater.TabIndex = 60;
            this.FreshWater.TabStop = true;
            this.FreshWater.Text = "Fresh";
            this.FreshWater.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 527);
            this.Controls.Add(this.AreaDetailsGroup);
            this.Controls.Add(this.RemoveAreaBtn);
            this.Controls.Add(this.AddAreaBtn);
            this.Controls.Add(this.NewAreaID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.NewAreaName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.AreaList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ZoneList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "DSP Fishing Area Manager";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.AreaDetailsGroup.ResumeLayout(false);
            this.AreaDetailsGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FishingAreaBounds)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox ZoneList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox AreaList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NewAreaName;
        private System.Windows.Forms.TextBox NewAreaID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button AddAreaBtn;
        private System.Windows.Forms.Button RemoveAreaBtn;
        private System.Windows.Forms.GroupBox AreaDetailsGroup;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton BoundTypePolygon;
        private System.Windows.Forms.RadioButton BoundTypeRadius;
        private System.Windows.Forms.RadioButton BoundTypeZone;
        private System.Windows.Forms.TextBox AreaID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox AreaName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DataGridView FishingAreaBounds;
        private System.Windows.Forms.DataGridViewTextBoxColumn X;
        private System.Windows.Forms.DataGridViewTextBoxColumn Z;
        private System.Windows.Forms.TextBox FishingAreaRadius;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox FishingAreaHeight;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox FishingAreaCenterZ;
        private System.Windows.Forms.TextBox FishingAreaCenterY;
        private System.Windows.Forms.TextBox FishingAreaCenterX;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button SaveAreaBtn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton SeaWater;
        private System.Windows.Forms.RadioButton FreshWater;
    }
}

