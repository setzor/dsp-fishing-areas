﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Runtime.InteropServices;

namespace DSPFishingAreas
{
    public partial class Form1 : Form
    {
        string DBServer;
        string DBPort;
        string DBUsername;
        string DBPassword;
        string DBase;
        bool DBConnected = false;
        string SelectedZone;
        string SelectedArea;

        MySqlConnection conn;

        [StructLayout(LayoutKind.Explicit, Size = 12, Pack = 1)]
        public struct areavector_t
        {

            [MarshalAs(UnmanagedType.R4)]
            [FieldOffset(0)]
            public float x;
            [MarshalAs(UnmanagedType.R4)]
            [FieldOffset(4)]
            public float y;
            [MarshalAs(UnmanagedType.R4)]
            [FieldOffset(8)]
            public float z;
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void ConnectDB()
        {
            string connStr = String.Concat("server=", DBServer, ";user=", DBUsername, ";database=", DBase, ";port=3306;password=", DBPassword);
            conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                DBConnected = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Concat("Databse not configured correctly."));
            }
        }

        private bool IsDBConnected()
        {
            return (conn.State.ToString() == "Open");
        }

        private void LoadZoneList()
        {
            if (!IsDBConnected())
            {
                ConnectDB();
            }
            if (IsDBConnected())
            {
                string sql = "SELECT zoneid,name FROM fishing_zone ORDER BY name ASC";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                ZoneList.Items.Clear();
                while (rdr.Read())
                {
                    if (rdr[1].ToString().Length > 4)
                    {
                        Item item = new Item();
                        item.strValue = rdr[0].ToString();
                        item.strText = rdr[1].ToString().Replace('_', ' ');
                        ZoneList.Items.Add(item);
                    }
                }
                rdr.Close();
            }
        }

        private void LoadFishingAreas(string zoneId)
        {
            if (!IsDBConnected())
            {
                ConnectDB();
            }
            if (IsDBConnected())
            {
                try
                {
                    string sql = String.Concat("SELECT areaid,name,bound_type,bound_height,bound_radius,bounds,center_x,center_y,center_z,water_type FROM fishing_area WHERE zoneid=", zoneId, " ORDER BY areaid ASC");
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    MySqlDataReader rdr = cmd.ExecuteReader();
                    AreaList.Items.Clear();

                    while (rdr.Read())
                    {
                        if (rdr[1].ToString().Length > 0)
                        {
                            Item item = new Item();
                            item.strValue = rdr[0].ToString();
                            item.strText = rdr[0].ToString() + ") " + rdr[1].ToString().Replace('_', ' ');
                            AreaList.Items.Add(item);
                        }
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to load zone areas details. " + ex.Message, "Database Error");
                }
            }
        }

        private void LoadFishingZoneArea(string zoneId, string areaId)
        {
            if (!IsDBConnected())
            {
                ConnectDB();
            }
            if (IsDBConnected())
            {
                try
                {
                    string sql = String.Concat("SELECT areaid,name,bound_type,bound_height,bound_radius,bounds,center_x,center_y,center_z,water_type FROM fishing_area WHERE zoneid=", zoneId, " AND areaid=", areaId);
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    MySqlDataReader rdr = cmd.ExecuteReader();
                    FishingAreaBounds.Rows.Clear();
                    if (rdr.HasRows)
                    {
                        AreaDetailsGroup.Enabled = true;
                        rdr.Read();
                        AreaID.Text = rdr[0].ToString();
                        AreaName.Text = rdr[1].ToString();
                        int AreaBoundType = Int32.Parse(rdr[2].ToString());
                        switch(AreaBoundType)
                        {
                            case 0:
                                BoundTypeZone.Checked = true;
                                break;
                            case 1:
                                BoundTypeRadius.Checked = true;
                                break;
                            case 2:
                                BoundTypePolygon.Checked = true;
                                break;
                        }

                        FishingAreaHeight.Text = rdr[3].ToString();
                        FishingAreaRadius.Text = rdr[4].ToString();

                        if (AreaBoundType == 2)
                        {
                            if (!(rdr[5] is System.DBNull)) {
                                byte[] binData = (byte[])rdr[5];
                                if (binData.Length >= 12)
                                {
                                    IntPtr ptr = Marshal.AllocHGlobal(12);
                                    int numBounds = binData.Length / 12;
                                    for (int i = 0; i < numBounds; i++)
                                    {
                                        areavector_t vec;
                                        Marshal.Copy(binData, 0, ptr, 12);
                                        IntPtr dataPointer = Marshal.UnsafeAddrOfPinnedArrayElement(binData, i * 12);
                                        vec = (areavector_t)Marshal.PtrToStructure(dataPointer, typeof(areavector_t));

                                        FishingAreaBounds.Rows.Add(vec.x, vec.z);
                                    }
                                    Marshal.FreeHGlobal(ptr);
                                }
                            }
                        }

                        FishingAreaCenterX.Text = rdr[6].ToString();
                        FishingAreaCenterY.Text = rdr[7].ToString();
                        FishingAreaCenterZ.Text = rdr[8].ToString();
                        int WaterType = Int32.Parse(rdr[9].ToString());
                        switch (WaterType)
                        {
                            case 0:
                                FreshWater.Checked = true;
                                break;
                            case 1:
                                SeaWater.Checked = true;
                                break;
                        }

                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to load zone area details. " + ex.Message, "Database Error");
                }
            }

        }

        private void AddFishingArea()
        {
            if (IsDBConnected())
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = conn;

                    cmd.CommandText = "INSERT INTO fishing_area(zoneid,areaid,name) VALUES(?zoneid,?areaid,?name)";
                    cmd.Parameters.Add("?zoneid", MySqlDbType.VarChar).Value = SelectedZone;
                    cmd.Parameters.Add("?areaid", MySqlDbType.VarChar).Value = NewAreaID.Text;
                    cmd.Parameters.Add("?name", MySqlDbType.VarChar).Value = NewAreaName.Text;
                    cmd.ExecuteNonQuery();
                    LoadFishingAreas(SelectedZone);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to add fishing area. " + ex.Message, "Database Error");
                }
            }
            else
            {
                MessageBox.Show("Unable to connect to DB", "Database Error");
            }
        }

        private void DeleteFishingArea()
        {
            if (IsDBConnected())
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = conn;

                    cmd.CommandText = "DELETE FROM fishing_area WHERE zoneid=?zoneid and areaid=?areaid";
                    cmd.Parameters.Add("?zoneid", MySqlDbType.VarChar).Value = SelectedZone;
                    cmd.Parameters.Add("?areaid", MySqlDbType.VarChar).Value = SelectedArea;
                    cmd.ExecuteNonQuery();
                    LoadFishingAreas(SelectedZone);
                    RemoveAreaBtn.Enabled = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to delete fishing area. " + ex.Message, "Database Error");
                }
            }
            else
            {
                MessageBox.Show("Unable to connect to DB", "Database Error");
            }
        }

        private void UpdateFishingArea()
        {
            if (IsDBConnected())
            {
                try
                {
                    int boundSize = (FishingAreaBounds.Rows.Count-1) * 12;
                    byte[] bounds = new byte[boundSize];
                    if (boundSize > 0)
                    {
                        for (int i=0;i<(FishingAreaBounds.Rows.Count-1);i++)
                        {
                            areavector_t vec;
                            vec.x = float.Parse(FishingAreaBounds.Rows[i].Cells[0].Value.ToString());
                            vec.y = 0.0f;
                            vec.z = float.Parse(FishingAreaBounds.Rows[i].Cells[1].Value.ToString());
                            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(vec));
                            Marshal.StructureToPtr(vec, ptr, true);
                            Marshal.Copy(ptr, bounds, i * 12, 12);
                            Marshal.FreeHGlobal(ptr);
                        }
                    }
                    
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "UPDATE fishing_area set " +
                            "areaid=?areaid," +
                            "name=?name," +
                            "bound_type=?boundtype," +
                            "bound_height=?boundheight," +
                            "bound_radius=?boundradius," +
                            "bounds=?bounds," +
                            "center_x=?centerx," +
                            "center_y=?centery," +
                            "center_z=?centerz," +
                            "water_type=?watertype " +
                            "WHERE zoneid=?zoneid AND areaid=?curareaid";
                        MySqlParameter areaIdParameter          = new MySqlParameter("?areaid", MySqlDbType.Int32, 5);
                        MySqlParameter areaNameParameter        = new MySqlParameter("?name", MySqlDbType.Text, 256);
                        MySqlParameter areaBoundTypeParameter   = new MySqlParameter("?boundtype", MySqlDbType.Int32, 2);
                        MySqlParameter areaBoundHeightParameter = new MySqlParameter("?boundheight", MySqlDbType.Int32, 3);
                        MySqlParameter areaBoundRadiusParameter = new MySqlParameter("?boundradius", MySqlDbType.Int32, 5);
                        MySqlParameter areaBoundsParameter      = new MySqlParameter("?bounds", MySqlDbType.Blob, bounds.Length);
                        MySqlParameter areaCenterXParameter     = new MySqlParameter("?centerx", MySqlDbType.Float, 7);
                        MySqlParameter areaCenterYParameter     = new MySqlParameter("?centery", MySqlDbType.Float, 7);
                        MySqlParameter areaCenterZParameter     = new MySqlParameter("?centerz", MySqlDbType.Float, 7);
                        MySqlParameter zoneIdParameter          = new MySqlParameter("?zoneid", MySqlDbType.Int32, 5);
                        MySqlParameter curAreaIdParameter      = new MySqlParameter("?curareaid", MySqlDbType.Int32, 5);
                        MySqlParameter waterTypeParameter = new MySqlParameter("?watertype", MySqlDbType.Int32, 2);

                        areaIdParameter.Value = AreaID.Text;
                        areaNameParameter.Value = AreaName.Text;
                        if (BoundTypeZone.Checked) areaBoundTypeParameter.Value = 0;
                        if (BoundTypeRadius.Checked) areaBoundTypeParameter.Value = 1;
                        if (BoundTypePolygon.Checked) areaBoundTypeParameter.Value = 2;
                        areaBoundHeightParameter.Value = FishingAreaHeight.Text;
                        areaBoundRadiusParameter.Value = FishingAreaRadius.Text;
                        areaBoundsParameter.Value = bounds;
                        areaCenterXParameter.Value = FishingAreaCenterX.Text;
                        areaCenterYParameter.Value = FishingAreaCenterY.Text;
                        areaCenterZParameter.Value = FishingAreaCenterZ.Text;
                        zoneIdParameter.Value = SelectedZone;
                        curAreaIdParameter.Value = SelectedArea;
                        waterTypeParameter.Value = (FreshWater.Checked == true ? 0 : 1);

                        cmd.Parameters.Add(areaIdParameter);
                        cmd.Parameters.Add(areaNameParameter);
                        cmd.Parameters.Add(areaBoundTypeParameter);
                        cmd.Parameters.Add(areaBoundHeightParameter);
                        cmd.Parameters.Add(areaBoundRadiusParameter);
                        cmd.Parameters.Add(areaBoundsParameter);
                        cmd.Parameters.Add(areaCenterXParameter);
                        cmd.Parameters.Add(areaCenterYParameter);
                        cmd.Parameters.Add(areaCenterZParameter);
                        cmd.Parameters.Add(zoneIdParameter);
                        cmd.Parameters.Add(curAreaIdParameter);
                        cmd.Parameters.Add(waterTypeParameter);

                        cmd.ExecuteNonQuery();
                    }
                    MessageBox.Show("Area Updated Successfully.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to save fish details. " + ex.Message, "Database Error");
                }
            }
            else
            {
                MessageBox.Show("Unable to connect to DB", "Database Error");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DBServer = ConfigurationManager.AppSettings["server"].ToString();
            DBPort = ConfigurationManager.AppSettings["port"].ToString();
            DBUsername = ConfigurationManager.AppSettings["username"].ToString();
            DBPassword = ConfigurationManager.AppSettings["password"].ToString();
            DBase = ConfigurationManager.AppSettings["database"].ToString();
            AddAreaBtn.Enabled = false;
            RemoveAreaBtn.Enabled = false;
            ConnectDB();
            if (IsDBConnected())
            {
                LoadZoneList();
            }
        }

        private void ZoneList_SelectedValueChanged(object sender, EventArgs e)
        {
            AreaDetailsGroup.Enabled = false;
            if (ZoneList.SelectedItem == null) return;
            var zone = (Item)ZoneList.SelectedItem;
            RemoveAreaBtn.Enabled = false;
            if (zone != null)
            {
                SelectedZone = zone.strValue;
                LoadFishingAreas(SelectedZone);
                AddAreaBtn.Enabled = true;
            }
            else
            {
                SelectedZone = String.Empty;
                AddAreaBtn.Enabled = false;
            }
        }

        private void AreaList_SelectedValueChanged(object sender, EventArgs e)
        {
            if (AreaList.SelectedItem == null) return;
            var area = (Item)AreaList.SelectedItem;
            if (area != null)
            {
                RemoveAreaBtn.Enabled = true;
                SelectedArea = area.strValue;
                LoadFishingZoneArea(SelectedZone,SelectedArea);
            }
        }

        private void AddAreaBtn_Click(object sender, EventArgs e)
        {
            if (SelectedZone != String.Empty)
            {
                AddFishingArea();
            }
        }

        private void RemoveAreaBtn_Click(object sender, EventArgs e)
        {
            if (SelectedArea != String.Empty)
            {
                DeleteFishingArea();
            }
        }

        private void SaveAreaBtn_Click(object sender, EventArgs e)
        {
            if (SelectedArea != String.Empty)
            {
                UpdateFishingArea();
                LoadFishingAreas(SelectedZone);
            }
        }
    }
}
