﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSPFishingAreas
{
    public class Item
    {
        public string strText;
        public string strValue;
        public override string ToString()
        {
            return this.strText;
        }
    }
}
